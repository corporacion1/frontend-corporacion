export class Permisos {
    constructor(
        public titulo: string,
        public url: string,
        public tipo: string,
        public icono: string,
        public posicion: string,
        public sistema: string
    ) { }
}

export class PermisosPorRoles {
    constructor(
        public rol: number,
        public permiso: string[]
    ) { }
}
