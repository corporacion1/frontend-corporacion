import { Component, OnInit, OnDestroy } from '@angular/core';
import { RecordarCuenta } from '../../models/actualizarContrasena';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../../services/usuario/usuario.service';
import swal from 'sweetalert2';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-recuperar-cuenta-cmp',
  templateUrl: './recuperar-cuenta.component.html'
})

export class RecuperarCuentaComponent implements OnInit, OnDestroy {

  public test: Date = new Date();
  public cuentaModel: RecordarCuenta;
  public loading = false;
	public version = environment.version;


  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _usuarioService: UsuarioService
  ) {
    this.cuentaModel = new RecordarCuenta(null);
  }


  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('lock-page');
    body.classList.add('off-canvas-sidebar');
    const card = document.getElementsByClassName('card')[0];
    setTimeout(function () {
      // after 1000 ms we add the class animated to the login/register card
      card.classList.remove('card-hidden');
    }, 700);
  }

  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('lock-page');
    body.classList.remove('off-canvas-sidebar');

  }

  recordarCuenta(c) {
/*
    this.loading = true;

    let cuenta = c.form.value.correo;

    this._usuarioService.recordarCuenta(cuenta, 1).subscribe(
      (response: any) => {

        this.loading = false;
        if (response.status !== 'error') {

          swal({
            title: '¡Hemos encontrados este registro!',
            html: 'Por favor confirma si los siguientes datos corresponden a tu información: </br> <strong>Nombre: </strong>' + response.nombres + ' ' + response.apellidos + '</br><strong>Correo: </strong>' + response.correo,
            type: 'question',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            buttonsStyling: true,
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
              this.loading = true;
              this._usuarioService.recordarCuenta(cuenta, 2).subscribe(
                (response: any) => {
                  this.loading = false;
                  if (response.status !== 'error') {
                    swal('¡Bien!', response.message.correoMsg, 'success').then(
                      (result: any) => {
                        if (result.value) {
                          this._router.navigate(['/']);
                          this.cuentaModel = new RecordarCuenta(null);
                        }
                      }
                    );
                  }
                }, error => {
                  this.loading = false;
                  swal('Falló!', 'Ha ocurrido un error inesperado, por favor intente más tarde', 'error');
                  console.error('[ERROR_CREAR_CONTACTO]: ', error);

                }
              );
            }else {
              this.cuentaModel = new RecordarCuenta(null);
            }
          });
        } else {
          swal('Ha ocurrido un error!', response.message, 'error');
        }
      }
    ), error => {
      this.loading = false;
      swal('Falló!', 'Ha ocurrido un error inesperado, por favor intente más tarde', 'error');
      console.error('[ERROR_CREAR_CONTACTO]: ', error);

    };*/

  }


  confirmarUsuario(nombre, apellido, correo) {

  }
}
