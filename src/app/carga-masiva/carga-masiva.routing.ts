import { Routes } from '@angular/router';

import { SubirUsuariosComponent } from './subir-usuarios/subir-usuarios.component';

export const CargaMasivaRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'usuarios',
				component: SubirUsuariosComponent
			}
		]
	}
];
