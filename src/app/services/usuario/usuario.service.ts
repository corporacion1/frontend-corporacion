import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import swal from "sweetalert2";
import { CambiarContrasenia, ConfirmarContrasenia } from '../../models/actualizarContrasena';
import { ActualizarUsuario, UsuarioConToken, UsuarioSinToken } from '../../models/usuarios';

@Injectable({
	providedIn: 'root'
})
export class UsuarioService {

	identity: any;
	_headers: any;
	headers: any;
	url: string = '';
	token: string;

	constructor(
		private http: HttpClient,
		private router: Router
	) {
		console.log('[Usuario services is ready]...');
	}

	async iniciarSesion(usuario: any, gettoken = null) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this._headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return await new Promise((resolve, reject) => {
			if (gettoken != null) {
				usuario.gettoken = "true";
			}
			this.url = this.comprobarEmpresaServicios(usuario.empresas);
			const json = JSON.stringify(usuario);
			const params = "json=" + json;
			this.http.post(this.url + "/login", params, { headers: this._headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async expirarToken() {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			this.http.get(this.url + "/validarToken", { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	cerrarSesion(): void {
		let opcionEmpresa = localStorage.getItem('business');
		localStorage.clear();
		localStorage.setItem('business', opcionEmpresa);

		swal({
			title: "Cerrar Sesión",
			text: "¿Esta seguro que desea salir del sistema?",
			type: "info",
			showCancelButton: true,
			confirmButtonClass: "btn btn-success",
			cancelButtonClass: "btn btn-danger",
			confirmButtonText: "Si",
			cancelButtonText: "Cancelar",
			buttonsStyling: true,
			reverseButtons: true
		}).then(result => {
			if (result.value) {
				this.token = "";
				this.identity = null;
				this.router.navigate(['/']);
			}
		});
	}

	getIdentity() {
		const identity = JSON.parse(localStorage.getItem('identity_emp'));

		if (identity && identity !== "undefined") this.identity = identity;
		else this.identity = null;

		return this.identity;
	}

	async getToken(): Promise<string> {
		const token = localStorage.getItem("token_emp");

		if (token && token !== "undefined") this.token = token;
		else this.token = null;

		return await this.token;
	}

	isToken() {
		const isToken = localStorage.getItem("token_emp");
		return isToken ? true : false;
	}

	terminarSesion(): void {
		this.token = "";
		this.identity = null;

		let opcionEmpresa = localStorage.getItem('business');
		localStorage.clear();
		localStorage.setItem('business', opcionEmpresa);
		this.router.navigate(['/']);
	}

	refresh(): void {
		window.location.reload();
	}

	async listarUsuarios(opc: any, page: any) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const params = "?pagination=" + page;
			this.http.get(this.url + "/usuario/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async buscarPorIdUsuario(opc: string, idUsuario: string) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const params = "?idUsuario=" + idUsuario;
			this.http.get(this.url + "/usuario/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async actualizarUsuarioVotacion(idUsuario: string, actualizarCliente: ActualizarUsuario) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(actualizarCliente);
			let params = "json=" + json + "&idUsuario=" + idUsuario;
			this.http.put(this.url + "/usuario", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async cambiarContrasenia(id: string, cambiarContrasenia: CambiarContrasenia) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(cambiarContrasenia);
			let params = "json=" + json;
			this.http.post(this.url + "/actualizarPassword/" + id, params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async confirmarContrasenia(confirmarContrasenia: ConfirmarContrasenia) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(confirmarContrasenia);
			let params = "json=" + json;
			this.http.post(this.url + "/confirmarPassword", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async registrarUsuarioSinToken(usuarioSinToken: UsuarioSinToken) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this._headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(usuarioSinToken);
			let params = "json=" + json;
			this.http.post(this.url + "/usuario", params, { headers: this._headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async registrarUsuarioConToken(usuarioConToken: UsuarioConToken) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(usuarioConToken);
			let params = "json=" + json;
			this.http.post(this.url + "/usuarioRegistro", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	comprobarEmpresaServicios(empresas: string): string {
		/* if (empresas === 'A') return environment.apiCrmEmpresa; */
		return environment.apiCrmEmpresa;
	}

	nombresEmpresa(empresas: string): string {
		/* if (empresas === 'A') return 'Empresa'; */
		return 'CORPOSOFT';
	}

	rutaNombreEmpresa(nombreEmpresa: string) {
		return '';
		/* switch (nombreEmpresa) {
			default:
				return 'empresa';
				break;
		} */
	}
}