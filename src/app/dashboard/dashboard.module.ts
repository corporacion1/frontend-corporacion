import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../md/md.module';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSistemaModule } from '../shared/loading-sistema/loading-sistema.module';
import { DashboardClienteComponent } from './dashboard-cliente/dashboard-cliente.component';

@NgModule({
    declarations: [
        DashboardComponent,
        DashboardClienteComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(DashboardRoutes),
        FormsModule,
        MaterialModule,
        ReactiveFormsModule,
        MdModule,
        LoadingModule,
        LoadingSistemaModule
    ],
    providers: [DatePipe]
})
export class DashboardModule { }