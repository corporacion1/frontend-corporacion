export const environment = {
  production: false,
  apiCrmEmpresa: 'http://backend-corporacion.local',
  correoEmpresa: 'info@empresa.com',
  telefonoEmpresa: '7000000',
  localImag: './assets/img/',
  localFiles: './assets/files/',
  serverImg: '/images/',
  project: false,
  version: 'v.1.0.0'
};
