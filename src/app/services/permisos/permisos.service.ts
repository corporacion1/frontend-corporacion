import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Permisos, PermisosPorRoles } from '../../models/permisos';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
	providedIn: 'root'
})
export class PermisosService {

	headers: any;
	url: string = '';
	token: string;

	constructor(
		private http: HttpClient,
		private _usuarioService: UsuarioService
	) {
		console.log('[Permisos services is ready]...');
	}

	async listarPermisos() {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			this.http.get(this.url + '/permisos', { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}
	
	async listarPermisosPorSistema() {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			this.http.get(this.url + '/permisoSistema', { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async listarPermisosTodos(page: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			this.http.get(this.url + '/permisosSistemas?pagination=' + page, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async crearPermisos(permisos: Permisos) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(permisos);
			let params = "json=" + json;
			this.http.post(this.url + '/permisos', params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async listarRoles() {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			this.http.get(this.url + '/roles', { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async crearRolPermisos(rolPermiso: PermisosPorRoles) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(rolPermiso);
			let params = "json=" + json;
			this.http.post(this.url + '/permisosRoles', params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async listarPermisosRol(page: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			this.http.get(this.url + '/permisosRoles?pagination=' + page, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async modificarPermisosRol(idPermiso: any, estado: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "estado=" + estado;
			this.http.put(this.url + '/permisosRoles/' + idPermiso, params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async modificarPermisos(idPermiso: any, permisos: Permisos) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(permisos);
			let params = "json=" + json;
			this.http.put(this.url + '/permisos/' + idPermiso, params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}
}