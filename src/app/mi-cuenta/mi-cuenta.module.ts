import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MiCuentaRoutes } from './mi-cuenta.routing';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';
import { LoadingModule } from '../shared/loading/loading.module';

@NgModule({
	declarations: [PerfilUsuarioComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(MiCuentaRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule
	],
	providers: [DatePipe]
})
export class MiCuentaModule {}