import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import swal from "sweetalert2";
import { UsuarioService } from '../services/usuario/usuario.service';

declare const $: any;

//Metadata
export interface RouteInfo {
	path: string;
	title: string;
	type: string;
	icontype: string;
	collapse?: string;
	children?: ChildrenItems[];
}

export interface ChildrenItems {
	path: string;
	title: string;
	ab: string;
	type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [
	{
		path: '/dashboard',
		title: 'Dashboard',
		type: 'link',
		icontype: 'dashboard'
	},
	{
		path: '/usuarios/listar',
		title: 'Usuarios',
		type: 'link',
		icontype: 'person'
	},
	{
		path: '/cargar-datos/usuarios',
		title: 'Subir Usuarios',
		type: 'link',
		icontype: 'cloud'
	},
	{
		path: '/parametrizacion/listar',
		title: 'Parametrizaciones',
		type: 'link',
		icontype: 'extension'
	}
];

@Component({
	selector: 'app-sidebar-cmp',
	templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
	public menuItems: any[];
	ps: any;
	public nombres = "Invitado";
	public apellidos = "Invitado";
	public infoUsuario: any;
	public menuPermisos = [];

	public urlImg = environment.localImag;
	public nombreEmpresa = '';
	public nombreSistema = '';
	public rolUsuario: any;
	
	constructor(public _usuarioService: UsuarioService, public _routerPag: Router,) {

		if (localStorage.getItem('identity_emp')) {
			let base_64_data = atob(localStorage.getItem('identity_emp'));
			let identityData = JSON.parse(base_64_data);
			let dataUsuario = identityData['data'];
			this.rolUsuario = identityData['data'].rol.valor;

			this.nombreEmpresa = _usuarioService.nombresEmpresa(localStorage.getItem('business'));
			this.infoUsuario = dataUsuario;
			this.nombres = this.infoUsuario.nombres;
			this.apellidos = this.infoUsuario.apellidos;
		}
	}

	isMobileMenu() {
		if ($(window).width() > 991) {
			return false;
		}
		return true;
	};

	ngOnInit() {
		this.menuItems = ROUTES.filter(menuItem => menuItem);
		
		if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
			const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
			this.ps = new PerfectScrollbar(elemSidebar);
		}
	}
	updatePS(): void {
		if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
			this.ps.update();
		}
	}
	isMac(): boolean {
		let bool = false;
		if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
			bool = true;
		}
		return bool;
	}
}
