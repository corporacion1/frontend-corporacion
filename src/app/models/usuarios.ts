export class UsuarioSinToken {
	constructor(
		public tipoDocumento: string,
		public documento: string,
		public nombres: string,
		public apellidos: string,
		public correo: string,
		public rol: string
	) { }
};

export class UsuarioConToken {
	constructor(
		public tipoDocumento: string,
		public documento: string,
		public nombres: string,
		public apellidos: string,
		public correo: string,
		public rol: string,
		public fechaExp: string
	) { }
};

export class ActualizarUsuario {
	constructor(
		public tipoDocumento: string,
		public documento: string,
		public nombres: string,
		public apellidos: string,
		public rol: string,
		public estado: string,
		public user: string,
		public correo: string,
		public fechaExp: string
	) { }
};
