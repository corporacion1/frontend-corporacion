import { Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { PricingComponent } from './pricing/pricing.component';
import { LoginComponent } from './login/login.component';
import { RecuperarCuentaComponent } from './recuperar-cuenta/recuperar-cuenta.component';

export const PagesRoutes: Routes = [

	{
		path: '',
		children: [
			{
				path: 'iniciar-sesion',
				component: LoginComponent
			}, {
				path: 'recordar-cuenta',
				component: RecuperarCuentaComponent
			}, {
				path: 'registrarse',
				component: RegisterComponent
			}
		]
	}
];