import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { LoginGuard } from './services/guards/login.guard';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RecuperarCuentaComponent } from './pages/recuperar-cuenta/recuperar-cuenta.component';
import { ActualizarPassComponent } from './mi-cuenta/actualizar-pass/actualizar-pass.component';

export const AppRoutes: Routes = [
	{
		path: '',
		redirectTo: 'iniciar-sesion',
		pathMatch: 'full',
	},
	{
		path: '',
		component: AuthLayoutComponent,
		children: [
			{
				path: 'iniciar-sesion',
				component: LoginComponent
			},{
				path: 'registrarse',
				component: RegisterComponent
			}, {
				path: 'recordar-cuenta',
				component: RecuperarCuentaComponent
			}, {
				path: '0081991b3c2227d29d7f8e4f71b8f0867befc79372fe89963ce0c8164056c09d/:idUsuario/:newPass',
				component: ActualizarPassComponent
			}
		]
	},
	{
		path: '',
		component: AdminLayoutComponent,
		canActivate: [LoginGuard],
		children: [
			{
				path: '',
				loadChildren: './dashboard/dashboard.module#DashboardModule'
			},
			{
				path: 'parametrizacion',
				loadChildren: './parametrizacion/parametrizacion.module#ParametrizacionModule'
			},
			{
				path: 'registros-sistema',
				loadChildren: './registros-sistema/registros-sistema.module#RegistrosSistemaModule'
			},
			{
				path: 'permisos',
				loadChildren: './permisos/permisos.module#PermisosModule'
			},
			{
				path: 'usuarios',
				loadChildren: './usuarios/usuarios.module#UsuariosModule'
			},
			{
				path: 'mi-cuenta',
				loadChildren: './mi-cuenta/mi-cuenta.module#MiCuentaModule'
			},
			{
				path: 'cargar-datos',
				loadChildren: './carga-masiva/carga-masiva.module#CargaMasivaModule'
			}
		]
	}
];
