import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ParametrizacionRoutes } from './parametrizacion.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { CrearParametrosComponent } from './crear-parametros/crear-parametros.component';
import { ListarParametrosComponent } from './listar-parametros/listar-parametros.component';

@NgModule({
	declarations: [CrearParametrosComponent, ListarParametrosComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(ParametrizacionRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class ParametrizacionModule { }
