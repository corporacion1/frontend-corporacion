import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PermisosRoutes } from './permisos.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { PermisosMenuComponent } from './permisos-menu/permisos-menu.component';
import { PermisosRolComponent } from './permisos-rol/permisos-rol.component';
import { PermisosListarComponent } from './permisos-listar/permisos-listar.component';

@NgModule({
	declarations: [PermisosMenuComponent, PermisosRolComponent, PermisosListarComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(PermisosRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class PermisosModule { }
