import { Routes } from '@angular/router';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';

export const MiCuentaRoutes: Routes = [
	{
		path: '',
		redirectTo: 'perfil',
		pathMatch: 'full',
	},
	{
		path: '',
		children: [ {
				path: 'perfil',
				component: PerfilUsuarioComponent
			} ]
	}
];
