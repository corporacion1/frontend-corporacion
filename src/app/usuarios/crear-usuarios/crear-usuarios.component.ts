import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UsuarioConToken } from "../../models/usuarios";
import swal from 'sweetalert2';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { PermisosService } from '../../services/permisos/permisos.service';

declare const $: any;

@Component({
	selector: 'app-crear-usuarios',
	templateUrl: './crear-usuarios.component.html',
	styleUrls: ['./crear-usuarios.component.css']
})
export class CrearUsuariosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	datosUsuarioForm: FormGroup;
	tipoDocumentosArray: any[] = [];
	/* tipoDocumentosArray: any[] = [
		{ nombre: 'Cédula Ciudadania', valor: 'CC' },
		{ nombre: 'NIT', valor: 'NIT' },
		{ nombre: 'Pasaporte', valor: 'PP' },
		{ nombre: 'Cédula Extranjería', valor: 'CE' },
	]; */
	//rolesExistentes: any;
	rolesExistentes: any[] = [
		{ nombre: 'Admin', valor: 'A' },
		{ nombre: 'Usuario', valor: 'U' }
	];

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _usuarioService: UsuarioService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService,
		private _permisosService: PermisosService,
	) { }

	ngOnInit() {
		this.datosUsuarioForm = this._formBuilder.group({
			tipoDoc: ['', Validators.required],
			documento: ['', [Validators.required, Validators.pattern("[0-9]{5,13}$")]],
			nombres: ['', [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]],
			apellidos: ['', [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]],
			rol: ['', Validators.required],
			correo: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
			fechaExp: ['', Validators.required]
		});
		if (localStorage.getItem('identity_emp')) {
			this.cargarInformacionSelect();
			//this.listarRolesExistentes();
		}
	}

	async listarRolesExistentes() {
		this.loading = true;
		const responseRoles = await this._permisosService.listarRoles();
		//console.log(responseRoles);
		this.loading = false;
		if (responseRoles['status'] == "success" && responseRoles['code'] == "200") {
			this.rolesExistentes = responseRoles['data'];
		}
		else if (responseRoles['status'] == "success" && responseRoles['code'] == "300") {
			swal('Advertencia!', responseRoles['message'], 'warning');
		}
		else if (responseRoles["status"] === "error" && responseRoles["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRoles['message']);
		}
		else {
			swal('¡Error!', responseRoles['message'], 'error');
		}
	}

	async onSubmitCrearUsuario() {
		this.loading = true;

		let usuario = new UsuarioConToken(
			this.datosUsuarioForm.value.tipoDoc,
			this.datosUsuarioForm.value.documento,
			this.datosUsuarioForm.value.nombres,
			this.datosUsuarioForm.value.apellidos,
			this.datosUsuarioForm.value.correo,
			this.datosUsuarioForm.value.rol,
			this.datosUsuarioForm.value.fechaExp
		); //console.log(usuario);

		const responseUsuario = await this._usuarioService.registrarUsuarioConToken(usuario);
		//console.log(responseUsuario);
		if (responseUsuario["status"] === "success" && responseUsuario["code"] === "200") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Bien!', responseUsuario["message"], 'OK', 'success');
			this.datosUsuarioForm.reset();
			(document.getElementById('documento') as HTMLInputElement).focus();
		}
		else if (responseUsuario["status"] === "success" && responseUsuario["code"] === "300") {
			this.loading = false;
			(document.getElementById('documento') as HTMLInputElement).focus();
			this._sweetAlertService.alertGeneral('Advertencia!', responseUsuario["message"], 'OK', 'warning');
		}
		else if (responseUsuario["status"] === "error" && responseUsuario["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseUsuario['message']);
		}
		else {
			this.loading = false;
			(document.getElementById('documento') as HTMLInputElement).focus();
			this._sweetAlertService.alertGeneral('¡Error!', responseUsuario["message"], 'OK', 'error');
		}
	}

	async cargarInformacionSelect() {
		let tipoDocumentosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');

		tipoDocumentosParametros['data'].forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		this._router.navigate(["usuarios/listar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
