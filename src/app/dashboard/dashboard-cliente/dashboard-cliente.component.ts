import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-dashboard-cliente',
	templateUrl: './dashboard-cliente.component.html',
	styleUrls: ['./dashboard-cliente.component.css']
})
export class DashboardClienteComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	nombres: string;
	nombre: string = 'Usuario';
	nombresEmpresas: string = '';

	constructor(
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_emp')) {

			let base_64_data = atob(localStorage.getItem('identity_emp'));
			let identityData = JSON.parse(base_64_data);

			let dataUsuario = identityData['data'];
			this.nombres = dataUsuario.nombres + ' ' + dataUsuario.apellidos;
			this.nombre = dataUsuario.rol.nombre;
		}
	}

	ngOnInit() { }

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}