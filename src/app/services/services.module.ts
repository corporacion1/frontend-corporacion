import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LoginGuard } from './guards/login.guard';
import { SweetAlertService } from './sweet-alert/sweet-alert.service';
import { UsuarioService } from './usuario/usuario.service';
import { AuditoriaServices } from './auditoria/auditoria.service';
import { ParametrizacionService } from './parametrizacion/parametrizacion.service';
import { PermisosService } from './permisos/permisos.service';

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		HttpClientModule
	],
	providers: [
		LoginGuard,
		SweetAlertService,
		UsuarioService,
		AuditoriaServices,
		ParametrizacionService,
		PermisosService
	]
})
export class ServicesModule { }