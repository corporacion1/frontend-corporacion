import { Routes } from '@angular/router';
import { PermisosMenuComponent } from './permisos-menu/permisos-menu.component';
import { PermisosRolComponent } from './permisos-rol/permisos-rol.component';
import { PermisosListarComponent } from './permisos-listar/permisos-listar.component';

export const PermisosRoutes: Routes = [
	
	{
		path: '',
		children: [
			{
				path: 'menu',
				component: PermisosMenuComponent
			},
			{
				path: 'rol',
				component: PermisosRolComponent
			},
			{
				path: 'listar',
				component: PermisosListarComponent
			}
		]
	}
];
