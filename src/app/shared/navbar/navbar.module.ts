import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './navbar.component';
import { LoadingSistemaModule } from '../loading-sistema/loading-sistema.module';

@NgModule({
    imports: [RouterModule, CommonModule, LoadingSistemaModule],
    declarations: [NavbarComponent],
    exports: [NavbarComponent]
})

export class NavbarModule { }
