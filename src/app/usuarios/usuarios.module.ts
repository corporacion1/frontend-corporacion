import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UsuariosRoutes } from './usuarios.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { CrearUsuariosComponent } from './crear-usuarios/crear-usuarios.component';
import { ListarUsuariosComponent } from './listar-usuarios/listar-usuarios.component';
import { ActualizarUsuarioComponent } from './actualizar-usuario/actualizar-usuario.component';

@NgModule({
	declarations: [CrearUsuariosComponent, ListarUsuariosComponent, ActualizarUsuarioComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(UsuariosRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class UsuariosModule { }
