import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
	selector: 'app-loading-sistema',
	templateUrl: './loading-sistema.component.html',
	styleUrls: ['./loading-sistema.component.css']
})
export class LoadingSistemaComponent implements OnInit {
	
	public localImg = environment.localImag;

	constructor() { }

	ngOnInit(): void {
	}

}
