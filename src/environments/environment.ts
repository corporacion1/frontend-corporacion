export const environment = {
  production: true,
  apiCrmEmpresa: 'http://backend-corporacion.local',
  correoEmpresa: 'info@empresa.com',
  telefonoEmpresa: '7000000',
  localImag: './assets/img/',
  localFiles: './assets/files/',
  serverImg: '/images/',
  project: true,
  version: 'v.1.0.0'
};