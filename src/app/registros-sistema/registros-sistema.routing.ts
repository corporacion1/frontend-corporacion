import { Routes } from '@angular/router';
import { AuditoriaComponent } from './auditoria/auditoria.component';
import { ExcepcionesComponent } from './excepciones/excepciones.component';

export const RegistrosSistemaRoutes: Routes = [
	
	{
		path: '',
		children: [
			{
				path: 'auditoria',
				component: AuditoriaComponent
			},
			{
				path: 'excepciones',
				component: ExcepcionesComponent
			}
		]
	}
];
