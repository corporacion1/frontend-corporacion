import { Routes } from '@angular/router';

import { CrearParametrosComponent } from './crear-parametros/crear-parametros.component';
import { ListarParametrosComponent } from './listar-parametros/listar-parametros.component';

export const ParametrizacionRoutes: Routes = [

	{
		path: '',
		children: [ {
			path: 'registrar',
			component: CrearParametrosComponent
		},{
			path: 'listar',
			component: ListarParametrosComponent
		}]
	}
];