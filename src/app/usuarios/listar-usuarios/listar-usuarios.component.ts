import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert2";
import { UsuarioService } from '../../services/usuario/usuario.service';
import { PermisosService } from '../../services/permisos/permisos.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-usuarios',
	templateUrl: './listar-usuarios.component.html',
	styleUrls: ['./listar-usuarios.component.css']
})
export class ListarUsuariosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	buscarUsuarioFormGroup: FormGroup;
	dataTable: TableData;
	usuarioArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _usuariosService: UsuarioService,
		private _permisosService: PermisosService,
		private _sweetAlertService: SweetAlertService
	) {
		this.datosTabla();
	}

	ngOnInit() {
		this.buscarUsuarioFormGroup = this._formBuilder.group({
			filtroUsuario: ['', Validators.required]
		});
		if (localStorage.getItem('identity_emp')) {
			this.listarUsuarios(1);
		}
	}

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento",
				"Nombres Completos",
				"Usuario",
				"Rol",
				"Correo",
				"Fecha Expedición Documento",
				"Estado",
				"Acciones"
			],
			dataRows: []
		};
	}

	async listarUsuarios(page: any) {
		this.loading = true;
		const responseUsuarios = await this._usuariosService.listarUsuarios('1', page);
		//console.log(responseUsuarios);
		if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "200") {
			this.loading = false;
			this.llenarDatosTabla(page, responseUsuarios['data']);
		}
		else if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "300") {
			this.loading = false;
			this.usuarioArray = [];
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
		else if (responseUsuarios["status"] === "error" && responseUsuarios["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseUsuarios['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
	}

	llenarDatosTabla(page: any, responseUsuarios: any) {
		this.usuarioArray = responseUsuarios['usuarios'];
		this.dataTable.dataRows = [];
		this.total_item_count = responseUsuarios['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseUsuarios['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseUsuarios['total_pages']) this.page_next = page + 1;
		else {
			if (responseUsuarios['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseUsuarios['total_pages'];
		}

		let JSONParam;

		this.usuarioArray.forEach(element => {
			JSONParam = {
				id: element.id,
				tipoDoc: element.tipoDocumento.valor,
				documento: element.documento,
				nombres: element.nombres + ' ' + element.apellidos,
				usuario: element.usuario == null ? '- - -' : element.usuario,
				rol_valor: element.rol.valor,
				rol: element.rol.nombre,
				correo: element.correo == null ? '- - -' : element.correo,
				estadoNombre: element.estado.nombre,
				estado: element.estado,
				fechaExp: element.fechaExp == null ? '- - -' : element.fechaExp
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		//console.log(this.usuarioArray);
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	buscarIdUsuario(rowId: string) {
		this._router.navigate(["usuarios/actualizar-informacion/", rowId]);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	crearUsuario() {
		this._router.navigate(["usuarios/registrar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}