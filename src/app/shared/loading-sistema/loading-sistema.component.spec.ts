import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingSistemaComponent } from './loading-sistema.component';

describe('LoadingSistemaComponent', () => {
  let component: LoadingSistemaComponent;
  let fixture: ComponentFixture<LoadingSistemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingSistemaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingSistemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
