import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingSistemaComponent } from './loading-sistema.component';

@NgModule({
  declarations: [LoadingSistemaComponent],
  imports: [
    CommonModule
  ],
  exports: [LoadingSistemaComponent]
})
export class LoadingSistemaModule { }
