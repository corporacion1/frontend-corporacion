import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../services/usuario/usuario.service';
import { SweetAlertService } from '../services/sweet-alert/sweet-alert.service';
import swal from 'sweetalert2';

declare var $: any;

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	cargaLoading: boolean = false;
	homeCliente: boolean = false;
	datosUsuario: any;
	nombresEmpresas: any = '';
	opcionVentana: any = '';
	cambioSistema: string = '';
	valorCambio: string = '';
	rolUsuario: string = '';

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_emp')) {

			let base_64_data = atob(localStorage.getItem('identity_emp'));
			let identityData = JSON.parse(base_64_data);
			let dataUsuario = identityData['data'];
			this.rolUsuario = dataUsuario.rol.valor;
		}
		else
			if (this.validarToken) { this.expiracionToken('Usuario no autenticado'); }
	}

	ngOnInit() { }

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
