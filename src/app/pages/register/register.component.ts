import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioSinToken } from '../../models/usuarios';
import swal from 'sweetalert2';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-register-cmp',
	templateUrl: './register.component.html'
})

export class RegisterComponent implements OnInit, OnDestroy {
	test: Date = new Date();
	version = environment.version;
	registroCliente: UsuarioSinToken;
	tipoDocArray: any[] = [
		{ nombre: 'Cedula Ciudadania', valor: 'CC' },
		{ nombre: 'Cedula Extranjería', valor: 'CC' },
		{ nombre: 'Pasaporte', valor: 'CC' }
	];
	loading = false;
	check: any;
	hide: boolean = true;

	constructor(
		private _router: Router,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService,
		private _route: ActivatedRoute,
	) {
		$("#terminosCondiciones").attr('checked', false);
		this.registroCliente = new UsuarioSinToken('', '', '', '', '', 'A');
		let opcionEmpresa = this._route.snapshot.data.opc;
		localStorage.setItem('business', opcionEmpresa);
	}

	ngOnInit() {
		const body = document.getElementsByTagName('body')[0];
		body.classList.add('register-page');
		body.classList.add('off-canvas-sidebar');
		let informacionEmpresa: string[] = this._sweetAlertService.datosEmpresas(localStorage.getItem('business'));
	}

	ngOnDestroy() {
		const body = document.getElementsByTagName('body')[0];
		body.classList.remove('register-page');
		body.classList.remove('off-canvas-sidebar');
	}

	async onSubmitRegistrarse(registrarClienteForm: UsuarioSinToken) {
		//console.log(this.registroCliente);
		if (this.check) {

			this.loading = true;
			const responseRegistrarCliente = await this._usuarioService.registrarUsuarioSinToken(this.registroCliente);
			//console.log(responseRegistrarCliente);
			if (responseRegistrarCliente["status"] === "success" && responseRegistrarCliente["code"] === "200") {
				this.loading = false;
				swal('¡Bien!', responseRegistrarCliente['message'], 'success');
				this._router.navigate(["/"]);
			}
			else if (responseRegistrarCliente["status"] === "success" && responseRegistrarCliente["code"] === "300") {
				this.loading = false;
				this._sweetAlertService.alertGeneral('Advertencia!', responseRegistrarCliente["message"], 'OK', 'warning');
			}
			else {
				this.loading = false;
				this._sweetAlertService.alertGeneral('¡Error!', responseRegistrarCliente["message"], 'OK', 'error');
			}

		} else {
			swal('Nos hace falta un paso  <i class="material-icons md-36">how_to_reg</i>',
				'Debes aceptar los terminos y condiciones de uso para continuar con el proceso de registro',
				'warning');
		}
	}

	aceptarAcuerdo() {
		this.check = true;
		$("#terminosCondiciones").attr('checked', true);
		$('#modalAceptarTerminos').modal('hide');
	}

	checkTerminos(event: any) {
		this.check = event.currentTarget.checked;
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}
}