export class Parametrizacion {
	constructor(
		public nombre: string,
		public valor: string,
		public categoria: string,
		public descripcion?: string
	) { }
}