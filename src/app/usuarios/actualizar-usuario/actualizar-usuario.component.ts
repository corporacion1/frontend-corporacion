import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { DatosUsuarioVotacion } from '../../interfaces/interfaces';
import { ActualizarUsuario } from '../../models/usuarios';
import { DatePipe } from "@angular/common";
import swal from "sweetalert2";
import { UsuarioService } from '../../services/usuario/usuario.service';
import { PermisosService } from '../../services/permisos/permisos.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-actualizar-usuario',
	templateUrl: './actualizar-usuario.component.html',
	styleUrls: ['./actualizar-usuario.component.css']
})
export class ActualizarUsuarioComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	actualizarUsuarioForm: FormGroup;
	//rolesExistentes: any;
	rolesExistentes: any[] = [
		{ nombre: 'Admin', valor: 'A' },
		{ nombre: 'Usuario', valor: 'U' },
	];
	idUsuario: any;
	tipoDocumentosArray: any[] = [];
	/* tipoDocumentosArray: any[] = [
		{ nombre: 'Cédula Ciudadania', valor: 'CC' },
		{ nombre: 'NIT', valor: 'NIT' },
		{ nombre: 'Pasaporte', valor: 'PP' },
		{ nombre: 'Cédula Extranjería', valor: 'CE' },
	]; */
	estadosArray: any[] = [];
	/* estadosArray: any[] = [
		{ nombre: 'Activo', valor: 'A' },
		{ nombre: 'Inactivo', valor: 'I' }
	]; */
	ocultarEdicionCampos: boolean = false;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _usuariosService: UsuarioService,
		private _permisosService: PermisosService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService,
		private datepipe: DatePipe
	) {
		this.idUsuario = this._route.snapshot.paramMap.get('idUsuario');
		if (localStorage.getItem('identity_emp')) {
			let base_64_data = atob(localStorage.getItem('identity_emp'));
			let identityData = JSON.parse(base_64_data);
			let permisoVerFicha = identityData['data'];

			if (permisoVerFicha.rol.valor == 'A') {
				this.ocultarEdicionCampos = true;
			}
		}
	}

	ngOnInit() {
		this.actualizarUsuarioForm = this._formBuilder.group({
			tipoDoc: ['', Validators.required],
			documento: ['', [Validators.required, Validators.pattern("[0-9]{5,13}$")]],
			nombres: ['', [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]],
			apellidos: ['', [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]],
			usuario: ['', Validators.required],
			rol: ['', Validators.required],
			estado: ['', Validators.required],
			correo: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
			fechaExp: ['', Validators.required]
		});
		if (localStorage.getItem('identity_emp')) {
			this.buscarIdCliente();
			this.cargarInformacionSelectCliente();
		}
	}

	async cargarInformacionSelectCliente() {
		this.loading = true;

		let tipoDocumentosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');
		let estadosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Estados');

		tipoDocumentosParametros['data'].forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		estadosParametros['data'].forEach((element) => {
			this.estadosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		//this.listarRolesExistentes();
	}

	async listarRolesExistentes() {
		this.loading = true;
		const responseRoles = await this._permisosService.listarRoles();
		//console.log(responseRoles);
		this.loading = false;
		if (responseRoles['status'] == "success" && responseRoles['code'] == "200") {
			this.rolesExistentes = responseRoles['data'];
		}
		else if (responseRoles['status'] == "success" && responseRoles['code'] == "300") {
			swal('Advertencia!', responseRoles['message'], 'warning');
		}
		else if (responseRoles["status"] === "error" && responseRoles["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRoles['message']);
		}
		else {
			swal('¡Error!', responseRoles['message'], 'error');
		}
	}

	/* Actualizacion de clientes e informacion personal */
	async buscarIdCliente() {
		this.loading = true;
		const responseIdCliente = await this._usuariosService.buscarPorIdUsuario('2', this.idUsuario);
		//console.log(responseIdCliente);
		if (responseIdCliente["status"] === "success" && responseIdCliente["code"] === "200") {
			let datosCliente: DatosUsuarioVotacion = responseIdCliente['data'];

			this.actualizarUsuarioForm.setValue({
				tipoDoc: datosCliente.tipoDocumento.valor,
				documento: datosCliente.documento,
				nombres: datosCliente.nombres,
				apellidos: datosCliente.apellidos,
				usuario: datosCliente.usuario,
				rol: datosCliente.rol.valor,
				estado: datosCliente.estado.valor,
				correo: datosCliente.correo,
				fechaExp: datosCliente.fechaExp,
			});
			this.loading = false;
		}
		else if (responseIdCliente["status"] === "success" && responseIdCliente["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseIdCliente["message"], 'OK', 'warning');
		}
		else if (responseIdCliente["status"] === "error" && responseIdCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseIdCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseIdCliente["message"], 'OK', 'error');
		}
	}

	async modificarUsuario() {
		//this.loading = true;
		let actualizarCliente = new ActualizarUsuario(
			this.actualizarUsuarioForm.value.tipoDoc,
			this.actualizarUsuarioForm.value.documento,
			this.actualizarUsuarioForm.value.nombres,
			this.actualizarUsuarioForm.value.apellidos,
			this.actualizarUsuarioForm.value.rol,
			this.actualizarUsuarioForm.value.estado,
			this.actualizarUsuarioForm.value.usuario,
			this.actualizarUsuarioForm.value.correo,
			this.fechasValidacion(this.actualizarUsuarioForm.value.fechaExp)
		); //console.log(actualizarCliente);

		let responseActualizarCliente = await this._usuariosService.actualizarUsuarioVotacion(this.idUsuario, actualizarCliente);
		//console.log(responseActualizarCliente);
		if (responseActualizarCliente["status"] === "success" && responseActualizarCliente["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarCliente["message"], 'success')
				.then(result => {
					this.actualizarUsuarioForm.reset();
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseActualizarCliente["status"] === "success" && responseActualizarCliente["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarCliente["message"], 'warning');
		}
		else if (responseActualizarCliente["status"] === "error" && responseActualizarCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarCliente['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarCliente["message"], 'error');
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	fechasValidacion(formatoFecha: any) {
		//console.log(formatoFecha);
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this.datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	regresar() {
		if (localStorage.getItem('ficha')) {
			let idIngreso = localStorage.getItem('idIngreso');
			localStorage.removeItem('ficha');
			this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
		}
		else { this._router.navigate(["usuarios/listar"]); }
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}