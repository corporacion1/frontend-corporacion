import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../md/md.module';

import { CargaMasivaRoutes } from './carga-masiva.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSistemaModule } from '../shared/loading-sistema/loading-sistema.module';
import { SubirUsuariosComponent } from './subir-usuarios/subir-usuarios.component';

@NgModule({
	declarations: [
		SubirUsuariosComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(CargaMasivaRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		MdModule,
		LoadingModule,
		LoadingSistemaModule
	],
	providers: [DatePipe]
})
export class CargaMasivaModule { }